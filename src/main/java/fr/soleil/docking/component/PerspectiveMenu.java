/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.docking.component;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

import fr.soleil.docking.action.NewPerspectiveAction;
import fr.soleil.docking.perspective.IPerspectiveFactory;

public class PerspectiveMenu extends javax.swing.JMenu implements PropertyChangeListener {

    private static final long serialVersionUID = -6492472049072385936L;

    protected IPerspectiveFactory factory;

    public PerspectiveMenu(IPerspectiveFactory factory) {
        super();
        this.setText("Perspective");
        this.factory = factory;
        factory.addPropertyChangeListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt != null) {
            if (evt.getSource() instanceof IPerspectiveFactory) {
                if (IPerspectiveFactory.PERSPECTIVES.equals(evt.getPropertyName())) {
                    // TODO
                }
            }
        }
    }

    @Override
    public JMenuItem add(Action a) {
        if (a instanceof NewPerspectiveAction) {
            this.addSeparator();
        }
        return super.add(a);
    }

    @Override
    public void setPopupMenuVisible(boolean flag) {
        if (flag) {
            this.removeAll();
            List<Action> actions = factory.getActionList();
            for (Action action : actions) {
                add(action);
                // JRadioButtonMenuItem item = (JRadioButtonMenuItem) this.add(action);
                // Boolean b = (Boolean) action.getValue("SELECTED_KEY");
                // if (b != null) {
                // item.setSelected(b);
                // }
            }
        }
        super.setPopupMenuVisible(flag);
    }

    @Override
    protected JMenuItem createActionComponent(Action a) {
        JMenuItem mi = new JRadioButtonMenuItem() {
            private static final long serialVersionUID = -3334691946468856486L;

            @Override
            protected PropertyChangeListener createActionPropertyChangeListener(Action a) {
                PropertyChangeListener pcl = createActionChangeListener(this);
                if (pcl == null) {
                    pcl = super.createActionPropertyChangeListener(a);
                }
                return pcl;
            }

        };
        mi.setHorizontalTextPosition(JButton.TRAILING);
        mi.setVerticalTextPosition(JButton.CENTER);
        return mi;
    }

}
