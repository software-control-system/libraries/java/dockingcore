/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.docking.view;

import java.awt.Component;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.prefs.Preferences;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;

import fr.soleil.docking.action.ViewAction;

/**
 * Basic implementation of {@link IViewFactory}
 * 
 * @author girardot
 */
public abstract class AbstractViewFactory implements IViewFactory {

    protected final Map<Object, IView> views;
    protected final PropertyChangeSupport support;

    public AbstractViewFactory() {
        super();
        views = new ConcurrentHashMap<Object, IView>();
        support = new PropertyChangeSupport(this);
    }

    @Override
    public IView getView(Object id) {
        IView result = null;
        if (id != null) {
            result = views.get(id);
        }
        return result;
    }

    public Component getViewComponent(Object id) {
        Component component;
        IView view = getView(id);
        if (view == null) {
            component = null;
        } else {
            component = view.getComponent();
        }
        return component;
    }

    protected void setViewEnabled(Object id, boolean enabled) {
        IView view = getView(id);
        if (view != null) {
            view.setVisible(enabled);
            view.setEnabled(enabled);
        }
    }

    @Override
    public void loadPreferences(Preferences prefs) {
        // Does nothing in particular. If you expect something to be done, you should extend this class and override
        // this method
    }

    @Override
    public void savePreferences(Preferences prefs) {
        // Does nothing in particular. If you expect something to be done, you should extend this class and override
        // this method
    }

    protected IView addView(IView view, boolean warnSupport) {
        IView added = null;
        if (view != null) {
            Object id = view.getId();
            if (id != null) {
                if (!views.containsKey(id)) {
                    views.put(id, view);
                    if (views.get(id) == view) {
                        added = view;
                    }
                }
            }
        }
        if (warnSupport && (added != null)) {
            synchronized (support) {
                support.firePropertyChange(VIEWS, null, view);
            }
        }
        return added;
    }

    @Override
    public IView addView(IView view) {
        return addView(view, true);
    }

    @Override
    public IView removeView(Object id) {
        IView toRemove;
        if (id == null) {
            toRemove = null;
        } else {
            toRemove = views.remove(id);
            if (toRemove != null) {
                synchronized (support) {
                    support.firePropertyChange(VIEWS, toRemove, null);
                }
            }
        }
        return toRemove;
    }

    @Override
    public IView addView(String title, Icon icon, Component component, Object id, JComponent dockingArea) {
        IView view = createView(title, icon, component, id);
        updateViewForDockingArea(view, dockingArea);
        return addView(view, dockingArea == null);
    }

    protected abstract IView createView(String title, Icon icon, Component component, Object id);

    protected abstract void updateViewForDockingArea(IView view, JComponent dockingArea);

    @Override
    public boolean removeView(IView view) {
        boolean result;
        if (view == null) {
            result = false;
        } else {
            Object id = view.getId();
            IView temp = getView(id);
            if (temp == view) {
                result = true;
                removeView(id);
            } else {
                result = false;
            }
        }
        return result;
    }

    @Override
    public List<Action> getActionList() {
        List<Action> result = new ArrayList<Action>(views.size());
        for (IView view : views.values()) {
            if (view != null) {
                result.add(new ViewAction(view));
            }
        }
        return result;
    }

    @Override
    public int size() {
        return views.size();
    }

    @Override
    public Collection<IView> getViews() {
        return views.values();
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        support.removePropertyChangeListener(listener);
    }

    @Override
    public Object readViewId(ObjectInputStream in) throws IOException {
        Object id = null;
        try {
            id = in.readObject();
        } catch (ClassNotFoundException e) {
            throw new IOException(e);
        }
        return id;
    }

    @Override
    public void writeViewId(Object id, ObjectOutputStream out) throws IOException {
        out.writeObject(id);
    }

}
