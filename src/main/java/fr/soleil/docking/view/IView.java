/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.docking.view;

import java.awt.Color;
import java.awt.Component;

import javax.swing.Icon;

import fr.soleil.docking.listener.IViewListener;

/**
 * A dynamically created view containing an id.
 * 
 * @author Hardion
 * @author GIRARDOT
 */
public interface IView {

    /**
     * Returns the view id.
     * 
     * @return the view id
     */
    public Object getId();

    /**
     * @return the enabled
     */
    public boolean isEnabled();

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled);

    /**
     * @return the enabled
     */
    public boolean isVisible();

    /**
     * @param enabled the enabled to set
     */
    public void setVisible(boolean visible);

    /**
     * Forces this {@link IView} to be selected
     */
    public void select();

    /**
     * @return the title
     */
    public String getTitle();

    /**
     * Sets this {@link IView}'s title
     * 
     * @param title The title to set
     */
    public void setTitle(String title);

    /**
     * @return the icon
     */
    public Icon getIcon();

    /**
     * Sets this {@link IView}'s icon
     * 
     * @param icon The icon to set
     */
    public void setIcon(Icon icon);

    /**
     * @return the component
     */
    public Component getComponent();

    /**
     * Returns this {@link IView}'s background {@link Color}
     * 
     * @return A {@link Color}
     */
    public Color getViewBackground();

    /**
     * Sets the background {@link Color} of this {@link IView}
     * 
     * @param bg The background {@link Color} to set
     */
    public void setViewBackground(Color bg);

    /**
     * Returns this {@link IView}'s foreground {@link Color}
     * 
     * @return A {@link Color}
     */
    public Color getViewForeground();

    /**
     * Sets the foreground {@link Color} of this {@link IView}
     * 
     * @param fg The foreground {@link Color} to set
     */
    public void setViewForeground(Color fg);

    /**
     * Sets the {@link IView} closable or not.
     * 
     * @param closable The boolean to set.
     */
    public void setClosable(boolean closable);

    /**
     * Add an {@link IViewListener} to the view..
     * 
     * @param listener The {@link IViewListener} to add.
     */
    public void addViewListener(IViewListener listener);

    /**
     * Removes an {@link IViewListener} from the view..
     * 
     * @param listener The {@link IViewListener} to remove.
     */
    public void removeViewListener(IViewListener listener);

}
