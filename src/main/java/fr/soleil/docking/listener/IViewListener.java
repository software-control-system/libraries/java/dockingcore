package fr.soleil.docking.listener;

import java.util.EventListener;

import fr.soleil.docking.event.ViewEvent;
import fr.soleil.docking.view.IView;

/**
 * An {@link EventListener} that listens to the changes in an {@link IView}
 * 
 * @author GIRARDOT
 */
public interface IViewListener extends EventListener {

    /**
     * Notifies this {@link IViewListener} for some changes in an {@link IView}
     * 
     * @param event The {@link ViewEvent} that describes the concerned {@link IView} and changes
     */
    public void viewChanged(ViewEvent event);

}
