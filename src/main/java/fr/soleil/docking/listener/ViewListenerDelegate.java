package fr.soleil.docking.listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.WeakHashMap;

import fr.soleil.docking.event.ViewEvent;

/**
 * A class delegated to {@link IViewListener}s management
 * 
 * @author GIRARDOT
 */
public class ViewListenerDelegate {

    protected final Collection<IViewListener> listeners;

    public ViewListenerDelegate() {
        listeners = Collections.newSetFromMap(new WeakHashMap<IViewListener, Boolean>());
    }

    /**
     * Warns all {@link IViewListener}s for some changes
     * 
     * @param event The {@link ViewEvent} that describes the changes
     */
    public void warnListeners(ViewEvent event) {
        List<IViewListener> copy;
        synchronized (listeners) {
            copy = new ArrayList<IViewListener>(listeners.size());
            copy.addAll(listeners);
        }
        for (IViewListener listener : copy) {
            if (listener != null) {
                listener.viewChanged(event);
            }
        }
        copy.clear();
    }

    /**
     * Adds a new {@link IViewListener}
     * 
     * @param listener the {@link IViewListener} to add
     */
    public void addViewListener(final IViewListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.add(listener);
            }
        }
    }

    /**
     * Removes an {@link IViewListener}
     * 
     * @param listener The {@link IViewListener} to remove
     */
    public void removeViewListener(IViewListener listener) {
        if (listener != null) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

}
