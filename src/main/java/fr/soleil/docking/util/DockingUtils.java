/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.docking.util;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.soleil.docking.view.IView;

/**
 * A class that gives some common useful methods about docking
 * 
 * @author girardot
 */
public class DockingUtils {

    /**
     * Generates an {@link AbstractAction} that can be used to display a given {@link IView}
     * 
     * @param view The concerned {@link IView}
     * @return An {@link AbstractAction}
     */
    public static AbstractAction generateShowViewAction(final IView view) {
        AbstractAction action = new AbstractAction(view.getTitle()) {

            private static final long serialVersionUID = 1387050898689913657L;

            @Override
            public void actionPerformed(ActionEvent e) {
                view.setVisible(true);
                view.select();
            }
        };
        action.putValue(AbstractAction.SMALL_ICON, view.getIcon());
        return action;
    }

}
