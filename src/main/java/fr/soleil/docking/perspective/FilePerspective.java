/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.docking.perspective;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Deprecated
public class FilePerspective extends Perspective {

    public FilePerspective(String name, File file) throws IOException {
        super(name);
        if ((file != null) && (file.isFile())) {
            InputStream in = new FileInputStream(file);
            if (in != null) {
                byteArray = PerspectiveFactory.readByteArray(in);
            }
        }
    }

}
