/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.docking.perspective;

import java.io.IOException;
import java.io.InputStream;

@Deprecated
public class ResourcePerspective extends Perspective {

    public ResourcePerspective(String name, String resource) throws IOException {
        super(name);
        InputStream in = ResourcePerspective.this.getClass().getResourceAsStream(resource);
        if (in != null) {
            byteArray = PerspectiveFactory.readByteArray(in);
        }
    }

}
