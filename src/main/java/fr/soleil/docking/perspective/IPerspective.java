/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.docking.perspective;

/**
 * Objects that implement this interface should know where to put each dockable component.
 * 
 * @author Hardion
 * @author GIRARDOT
 */
public interface IPerspective {

    public String getName();

    /**
     * @return the byteArray
     */
    public byte[] getByteArray();

    /**
     * @param byteArray the byteArray to set
     */
    public void setByteArray(byte[] byteArray);
}
