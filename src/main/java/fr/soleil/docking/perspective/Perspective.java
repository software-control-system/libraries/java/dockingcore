/**
 * 
 */
package fr.soleil.docking.perspective;


public class Perspective implements IPerspective {
    private final String name;
    protected byte[] byteArray;

    public Perspective(String name) {
        this.name = name;
        byteArray = new byte[0];
    }

    @Override
    public byte[] getByteArray() {
        return byteArray;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setByteArray(byte[] byteArray) {
        this.byteArray = byteArray;
    }

    @Override
    public String toString() {
        return this.name;
    }

}