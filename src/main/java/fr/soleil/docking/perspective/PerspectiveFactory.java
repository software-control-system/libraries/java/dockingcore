/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.docking.perspective;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.Action;

import fr.soleil.docking.action.NewPerspectiveAction;
import fr.soleil.docking.action.RemovePerspectiveAction;
import fr.soleil.docking.action.SelectPerspectiveAction;
import fr.soleil.docking.exception.DockingException;
import fr.soleil.lib.project.ObjectUtils;

public class PerspectiveFactory implements IPerspectiveFactory {

    private static final String DEFAULT_PERSPECTIVE_NAME = "Default";

    protected PropertyChangeSupport support;
    private final List<IPerspective> perspectives;
    private IPerspective selectedPerspective;
    private final String defaultPerspectiveName;

    public PerspectiveFactory() {
        this(new Perspective(DEFAULT_PERSPECTIVE_NAME));
    }

    public PerspectiveFactory(IPerspective defaultt) {
        perspectives = new ArrayList<IPerspective>(1);
        defaultPerspectiveName = defaultt.getName();
        perspectives.add(defaultt);
        selectedPerspective = defaultt;
        support = new PropertyChangeSupport(this);
    }

    @Override
    public IPerspective createPerspective(final String name) {
        IPerspective result = this.getPerspective(name);
        if (result == null) {
            result = new Perspective(name);
            this.add(result);
        }

        return result;
    }

    /**
     * @param e
     * @return
     * @see java.util.ArrayList#add(java.lang.Object)
     */
    public boolean add(IPerspective e) {
        boolean result = perspectives.add(e);
        if (result) {
            support.fireIndexedPropertyChange(PERSPECTIVES, perspectives.indexOf(e), null, result);
        }

        return result;
    }

    @Override
    public IPerspective getPerspective(Object id) {
        IPerspective result = null;
        if (id instanceof String) {
            String name = (String) id;
            for (IPerspective perspective : perspectives) {
                if (name.equalsIgnoreCase(perspective.getName())) {
                    result = perspective;
                    break;
                }
            }
        }
        return result;
    }

    @Override
    public IPerspective getSelectedPerspective() {
        return selectedPerspective;
    }

    @Override
    public void setSelectedPerspective(IPerspective selectedPerspective) {
        if ((selectedPerspective != null) && (!selectedPerspective.equals(this.selectedPerspective))
                && perspectives.contains(selectedPerspective)) {
            IPerspective old = this.selectedPerspective;
            this.selectedPerspective = selectedPerspective;
            support.firePropertyChange(SELECTED_PERSPECTIVE, old, this.selectedPerspective);
        }
    }

    @Override
    public void setSelectedPerspective(String perspective) {
        setSelectedPerspective(getPerspective(perspective));
    }

    @Override
    public void loadPreferences(Preferences prefs) {
        Preferences factoryPrefs = prefs.node("PerspectiveFactory");
        Preferences perspectivesPrefs = factoryPrefs.node("Perspectives");

        String[] keys = new String[0];
        try {
            keys = perspectivesPrefs.keys();
        } catch (BackingStoreException e1) {
            e1.printStackTrace();
        }

        byte[] ko = new byte[0];
        byte[] layout = null;
        IPerspective perspective = null;
        for (int i = 0; i < keys.length; i++) {
            perspective = this.getPerspective(keys[i]);
            if (perspective == null) {
                perspective = this.createPerspective(keys[i]);
            }
            layout = perspectivesPrefs.getByteArray(keys[i], ko);
            if (layout != ko) {
                perspective.setByteArray(layout);
            }
        }

        // get the value of
        String selected = factoryPrefs.get("selected", selectedPerspective.getName());
        this.setSelectedPerspective(this.getPerspective(selected));

    }

    @Override
    public void savePreferences(Preferences prefs) {
        Preferences factoryPrefs = prefs.node("PerspectiveFactory");
        Preferences perspectivesPrefs = factoryPrefs.node("Perspectives");

        try {
            factoryPrefs.clear();
            perspectivesPrefs.clear();
        } catch (BackingStoreException e1) {
            e1.printStackTrace();
        }
        factoryPrefs.put("selected", selectedPerspective.getName());
        for (IPerspective perspective : perspectives) {

            perspectivesPrefs.putByteArray(perspective.getName(), perspective.getByteArray());
        }
    }

    @Override
    public List<Action> getActionList() {
        List<Action> result = new ArrayList<Action>(1);

        for (IPerspective perspective : perspectives) {
            result.add(new SelectPerspectiveAction(this, perspective));
        }

        result.add(new NewPerspectiveAction(this));
        result.add(new RemovePerspectiveAction(this));

        return result;
    }

    @Override
    public void saveSelected(File file) throws DockingException {
        savePerspective(file, selectedPerspective);
    }

    @Override
    public void savePerspective(File file, IPerspective perspective) throws DockingException {
        if ((file != null) && (perspective != null)) {
            DockingException dockingException = null;
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(file);
                try {
                    out.write(perspective.getByteArray());
                } catch (Exception e) {
                    if (dockingException == null) {
                        dockingException = new DockingException("Error during file writing", e);
                    }
                } finally {
                    try {
                        out.close();
                    } catch (Exception e) {
                        if (dockingException == null) {
                            dockingException = new DockingException("Error while releasing file access", e);
                        }
                    }
                }
            } catch (Exception e) {
                if (dockingException == null) {
                    dockingException = new DockingException("Error during file access", e);
                }
            }
            if (dockingException != null) {
                throw dockingException;
            }
        }
    }

    /**
     * @param listener
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(java.beans.PropertyChangeListener)
     */
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }

    /**
     * @param listener
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener(java.beans.PropertyChangeListener)
     */
    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        support.removePropertyChangeListener(listener);
    }

    @Override
    public IPerspective getDefault() {

        return this.getPerspective(defaultPerspectiveName);
    }

    @Override
    public IPerspective[] getPerspectives() {
        return perspectives.toArray(new IPerspective[perspectives.size()]);
    }

    @Override
    public IPerspective removePerspective(final IPerspective p) {
        int index;
        if (p == null) {
            index = -1;
        } else {
            index = perspectives.indexOf(p);
        }
        if ((index > -1) && (!ObjectUtils.sameObject(p.getName(), defaultPerspectiveName))) {
            // Exist and it's not the default perspective
            perspectives.remove(index);
            support.fireIndexedPropertyChange(PERSPECTIVES, index, p, null);
        }
        return p;
    }

    public IPerspective removePerspective(final String name) {
        return this.removePerspective(this.getPerspective(name));
    }

    /**
     * Reads a <code>byte</code> array from an {@link InputStream} and returns it
     * 
     * @param in The {@link InputStream}
     * @return A <code>byte[]</code>
     * @throws IOException If a problem occurred while trying to read the {@link InputStream}
     */
    protected static byte[] readByteArray(InputStream in) throws IOException {
        byte[] result = null;
        if (in != null) {
            result = new byte[in.available()];
            int length = in.read(result);
            if (length != result.length) {
                result = null;
            }
        }
        return result;
    }

    @Override
    public IPerspective loadPerspectiveFromFile(File perspectiveFile, String perpectiveName) throws DockingException {
        Perspective perspective;
        if (perpectiveName == null) {
            perspective = null;
        } else {
            perspective = new Perspective(perpectiveName);
        }
        loadFileInPerspective(perspectiveFile, perspective);
        return perspective;
    }

    @Override
    public void loadFileInPerspective(File perspectiveFile, IPerspective perspective) throws DockingException {
        if ((perspectiveFile != null) && perspectiveFile.isFile() && (perspective != null)) {
            try {
                perspective.setByteArray(readByteArray(new FileInputStream(perspectiveFile)));
            } catch (Exception e) {
                throw new DockingException("Error during file reading", e);
            }
        }
    }

    @Override
    public IPerspective loadPerspectiveFromResource(String resource, String perpectiveName) throws DockingException {
        Perspective perspective;
        if (perpectiveName == null) {
            perspective = null;
        } else {
            perspective = new Perspective(perpectiveName);
        }
        loadResourceInPerspective(resource, perspective);
        return perspective;
    }

    @Override
    public void loadResourceInPerspective(String resource, IPerspective perspective) throws DockingException {
        if ((resource != null) && (!resource.trim().isEmpty()) && (perspective != null)) {
            try {
                perspective.setByteArray(readByteArray(getClass().getResourceAsStream(resource)));
            } catch (Exception e) {
                throw new DockingException("Error during resource reading", e);
            }
        }
    }

}
