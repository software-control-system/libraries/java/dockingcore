/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.docking.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.soleil.docking.view.IView;

public class ViewAction extends AbstractAction {

    private static final long serialVersionUID = 2102313023512752659L;

    private final IView view;

    public ViewAction(IView view) {
        super();
        this.view = view;
        // This is an instance initializer; it is executed just after the
        // constructor of the superclass is invoked

        // The following values are completely optional
        putValue(NAME, view.getTitle());
        // Set tool tip text
        putValue(SHORT_DESCRIPTION, view.getTitle());

        // This text is not directly used by any Swing component;
        // however, this text could be used in a help system
        putValue(LONG_DESCRIPTION, view.getTitle());

        // Set an icon
        // Icon icon = new ImageIcon("icon.gif");
        putValue(SMALL_ICON, view.getIcon());

        // Set a mnemonic character. In most look and feels, this causes the
        // specified character to be underlined This indicates that if the component
        // using this action has the focus and In some look and feels, this causes
        // the specified character in the label to be underlined and
        // putValue(Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_N));

        // Set an accelerator key; this value is used by menu items
        // putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt shift N"));

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!view.isVisible()) {
            view.setVisible(true);
        } else {
            view.setVisible(false);
        }
    }

    @Override
    public boolean isEnabled() {
        return view.isEnabled();
    }

}
