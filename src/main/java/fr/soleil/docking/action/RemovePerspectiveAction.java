/*
 * Created on 10 juin 2005
 * with Eclipse
 */
package fr.soleil.docking.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import fr.soleil.docking.perspective.IPerspective;
import fr.soleil.docking.perspective.IPerspectiveFactory;

public class RemovePerspectiveAction extends AbstractAction {

    private static final long serialVersionUID = 4431356413525394103L;

    private final IPerspectiveFactory factory;

    public RemovePerspectiveAction(IPerspectiveFactory factory) {
        super();

        this.factory = factory;

        // This is an instance initializer; it is executed just after the
        // constructor of the superclass is invoked

        // The following values are completely optional
        putValue(NAME, "Remove");
        // Set tool tip text
        putValue(SHORT_DESCRIPTION, "Remove Perspective");

        // This text is not directly used by any Swing component;
        // however, this text could be used in a help system
        putValue(LONG_DESCRIPTION, "Removing a perspective");

        // Set an icon
        // Icon icon = new ImageIcon("icon.gif");
        // putValue(Action.SMALL_ICON, icon);

        // Set a mnemonic character. In most look and feels, this causes the
        // specified character to be underlined This indicates that if the component
        // using this action has the focus and In some look and feels, this causes
        // the specified character in the label to be underlined and
        putValue(MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_R));

        // Set an accelerator key; this value is used by menu items
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl shift R"));

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        IPerspective result = (IPerspective) JOptionPane.showInputDialog(null, "Select the perspective to remove",
                "Remove perspective", JOptionPane.PLAIN_MESSAGE, null, factory.getPerspectives(),
                factory.getSelectedPerspective());
        if (result != null) {
            factory.removePerspective(result);
        }
    }

}