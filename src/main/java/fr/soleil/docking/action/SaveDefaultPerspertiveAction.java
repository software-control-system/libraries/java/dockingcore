/*
 * Created on 10 juin 2005
 * with Eclipse
 */
package fr.soleil.docking.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import fr.soleil.docking.ADockingManager;
import fr.soleil.lib.project.ObjectUtils;

public class SaveDefaultPerspertiveAction extends AbstractAction {

    private static final long serialVersionUID = -5058889377558638684L;

    private final ADockingManager dockingManager;

    public SaveDefaultPerspertiveAction(ADockingManager manager) {
        super();
        this.dockingManager = manager;
        // This is an instance initializer; it is executed just after the
        // constructor of the superclass is invoked

        // The following values are completely optional
        putValue(NAME, "Save Default");
        // Set tool tip text
        putValue(SHORT_DESCRIPTION, "Save this perspective to file system");

        // This text is not directly used by any Swing component;
        // however, this text could be used in a help system
        putValue(LONG_DESCRIPTION, "Save this perspective to file system");

        // Set an icon
        // Icon icon = new ImageIcon("icon.gif");
        // putValue(Action.SMALL_ICON, view.getIcon());

        // Set a mnemonic character. In most look and feels, this causes the
        // specified character to be underlined This indicates that if the component
        // using this action has the focus and In some look and feels, this causes
        // the specified character in the label to be underlined and
        // putValue(Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_N));

        // Set an accelerator key; this value is used by menu items
        // putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt shift N"));

    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        JFileChooser chooser = new JFileChooser();

        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();
            try {
                if (!file.exists())
                    file.createNewFile();
                dockingManager.saveDefault(file);
            } catch (Exception e) {
                JOptionPane.showMessageDialog((Component) evt.getSource(), ObjectUtils.printStackTrace(e));
            }
        }
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}