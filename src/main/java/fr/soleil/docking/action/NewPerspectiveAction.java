/*
 * Created on 10 juin 2005
 * with Eclipse
 */
package fr.soleil.docking.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import fr.soleil.docking.perspective.IPerspective;
import fr.soleil.docking.perspective.IPerspectiveFactory;

public class NewPerspectiveAction extends AbstractAction {

    private static final long serialVersionUID = -3295304363885371351L;

    private final IPerspectiveFactory factory;

    public NewPerspectiveAction(IPerspectiveFactory factory) {
        super();

        this.factory = factory;

        // This is an instance initializer; it is executed just after the
        // constructor of the superclass is invoked

        // The following values are completely optional
        putValue(NAME, "New");
        // Set tool tip text
        putValue(SHORT_DESCRIPTION, "New Perspective");

        // This text is not directly used by any Swing component;
        // however, this text could be used in a help system
        putValue(LONG_DESCRIPTION, "Adding a new perspective");

        // Set an icon
        // Icon icon = new ImageIcon("icon.gif");
        // putValue(Action.SMALL_ICON, icon);

        // Set a mnemonic character. In most look and feels, this causes the
        // specified character to be underlined This indicates that if the component
        // using this action has the focus and In some look and feels, this causes
        // the specified character in the label to be underlined and
        putValue(MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_N));

        // Set an accelerator key; this value is used by menu items
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl shift N"));

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        IPerspective current = factory.getSelectedPerspective();
        String result = JOptionPane.showInputDialog("Enter the name of the new perspective");
        if ((result != null) && (!result.trim().isEmpty())) {
            IPerspective p = factory.createPerspective(result);
            byte[] array = current.getByteArray();
            byte[] copy = (array == null ? null : array.clone());
            p.setByteArray(copy);
            factory.setSelectedPerspective(p);
        }
    }

}