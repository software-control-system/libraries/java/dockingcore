/*******************************************************************************
 * Copyright (c) 2008-2019 Synchrotron SOLEIL
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 ******************************************************************************/
package fr.soleil.docking.exception;

/**
 * An Exception thrown in case of problems with docking
 * 
 * @author girardot
 */
public class DockingException extends Exception {

    private static final long serialVersionUID = 5753611012723925293L;

    public DockingException() {
        super();
    }

    public DockingException(String message) {
        super(message);
    }

    public DockingException(Throwable cause) {
        super(cause);
    }

    public DockingException(String message, Throwable cause) {
        super(message, cause);
    }

}
