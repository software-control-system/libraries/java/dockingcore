package fr.soleil.docking.event;

import java.util.EventObject;

import fr.soleil.docking.view.IView;

/**
 * An {@link EventObject} that describes the changes in an {@link IView}
 * 
 * @author GIRARDOT
 */
public class ViewEvent extends EventObject {

    private static final long serialVersionUID = 1725475008782759147L;

    /**
     * An <code>int</code> used to notify that the associated view was closed
     */
    public static final int VIEW_CLOSED = 0;
    /**
     * An <code>int</code> used to notify that the associated view gained the focus
     */
    public static final int FOCUS_GAINED = 1;
    /**
     * An <code>int</code> used to notify that the associated view lost the focus
     */
    public static final int FOCUS_LOST = 2;

    protected final int reason;

    public ViewEvent(IView source, int reason) {
        super(source);
        this.reason = reason;
    }

    @Override
    public IView getSource() {
        return (IView) super.getSource();
    }

    /**
     * Returns the reason of the changes
     * 
     * @return An <code>int</code>
     * @see #VIEW_CLOSED
     * @see #FOCUS_GAINED
     * @see #FOCUS_LOST
     */
    public int getReason() {
        return reason;
    }

}
